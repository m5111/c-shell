#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pwd.h>
#include <string.h>
#include <wait.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>

char *username;
char hostname[1024];
char pwd[1024];
FILE *history;
char historyDirectory[1024];
int lastHistoryNumber;

typedef struct command {
    char *command;
    char **args;
} command_t;

typedef int (*func)(char **);

typedef struct node {
    char *name;
    func function;
    struct node *next;
} node_t;

node_t *commandListHead;

void initNode(node_t *node, char name[], func function, node_t *next) {
    node->name = name;
    node->function = function;
    node->next = next;
}

void initHistory() {
    strcat(historyDirectory, getenv("HOME"));
    strcat(historyDirectory, "/.mysh_history");
    history = fopen(historyDirectory, "a+");
    char readLine[1024];
    while (fgets(readLine, 1024, history));
    char number[1024];
    unsigned long length = strchr(readLine, ' ') -readLine;
    memcpy(number, readLine, length);
    number[length] = 0;
    lastHistoryNumber = atoi(number);
    fclose(history);
}

char *getUserName() {
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);
    if (pw) {
        return pw->pw_name;
    }
    return "";
}

char *readLine() {
    char *line;
    size_t bufferSize = 0;
    getline(&line, &bufferSize, stdin);
    return line;
}

//Funckje wbudowane w powloke:


int funcExit(char **args) {
    if (args[1] == NULL) exit(0);
    else exit(atoi(args[1]));
}

int funcVersion(char **args) {
    printf("\nmysh\nTomasz Nowak s439453\nv1.0\n\n");
    return 0;
}

int funcCD(char **args) {
    int result;
    if (args[1] == NULL) {
        result = chdir("..");
    } else {
        result = chdir(args[1]);
    }
    if (result == -1) {
        int error = errno;
        printf("%s\n", strerror(error));
        return -1;
    }
    getcwd(pwd, 1024);
    return 0;
}

int funcHelp(char **args) {
    printf("\nWBUDOWANE POLECENIA POWLOKI:\n\n");
    printf("ZMIENNA=WARTOSC - Ustawienie zmiennej srodowiskowej.\n");
    printf("    $ZMIENNA - Odczytywanie wartosci zmiennej srodowiskowej.\n\n");
    printf("history [-c] - wyswietla historie wpisanych polecen.\n");
    printf("    -c - kasuje historie wpisanych polecen.\n\n");
    printf("fc [-l] [pierwszy] [ostatni] - wyswietla, edytuje i wykonuje polecenia z listy historii.\n");
    printf("    -l - wypisywanie wierszy zamiast ich edycji.\n\n");
    printf("cd - zmiana katalogu biezacego.\n\n");
    printf("version - wyswietla informacje o autorze.\n\n");
    printf("exit - konczy dzialanie powloki z podanym kodem powrotu.\n\n");
    printf("help - wyswietla pomoc.\n\n");
    return 0;
}

int funcHistory(char **args) {
    if (args[1] != NULL && strcmp(args[1], "-c") == 0) {
        fclose(fopen(historyDirectory, "w"));
        printf("Historia zostala wyczyszczona.\n");
        lastHistoryNumber = 0;
    } else {
        char *args2[2];
        args2[0] = "cat";
        args2[1] = historyDirectory;
        args2[2] = NULL;
        funcExecvp(args2);
    }
    return 0;
}

int funcFC(char **args) {
    int i = 1;
    int numbers[2] = {NULL, NULL};
    int j = 0;
    while (args[i] != NULL && j != 2) {
        if (isNumber(args[i])) {
            numbers[j] = atoi(args[i]);
            j++;
        }
        i++;
    }
    i = 1;
    while (args[i] != NULL) {
        if (strcmp(args[i], "-l") == 0) {
            if (numbers[0] != NULL) {
                if (numbers[1] != NULL) {
                    printHistoryRange(numbers[0], numbers[1]);
                } else {
                    printHistoryRange(numbers[0], lastHistoryNumber);
                }
            } else {
                printHistoryRange(lastHistoryNumber - 17, lastHistoryNumber - 1);
            }
            return 0;
        }
        i++;
    }
    if (numbers[0] != NULL) {
        if (numbers[1] != NULL) {
            saveHistoryRange(numbers[0], numbers[1]);
        } else {
            saveHistoryRange(numbers[0], lastHistoryNumber);
        }
    } else {
        saveHistoryRange(lastHistoryNumber - 1, lastHistoryNumber - 1);
    }
    return 0;
}

int funcExecvp(char **args) {
    int exitStatus;
    int pid = fork();
    if (pid == -1) {
        int error = errno;
        printf("%s\n", strerror(error));
        return -1;
    }
    if (pid == 0) {
        execvp(args[0], args);
        exit(127);
    } else {
        waitpid(pid, &exitStatus, 0);
        return WEXITSTATUS(exitStatus);
    }
}

//Przeszukiwanie listy polecen.
int commandFinder(command_t command) {
    if (command.command[0] == '\0') {
        return 0;
    }
    //Pokazywanie wartosc zmiennej
    int j = 1;
    while (command.args[j] != NULL) {
        char *pointer = strchr(command.args[j], '$');
        if (pointer) {
            char* variable = getenv(pointer + 1);
            strcpy(command.args[j],variable);
        }
        j++;
    }

    //Zmienne srodowiskowe
    char *pointer = strchr(command.command, '=');
    if (pointer) {
        char variable[1024];
        char *value;
        unsigned long length = pointer-command.command;
        memcpy(variable,command.command,length);
        variable[length] = 0;
        value = pointer + 1;
        return setenv(variable, value, 1);
    }
    //Nasze polecenia
    node_t *node = commandListHead;
    while (node != NULL) {
        if (strcmp(command.command, node->name) == 0) {
            return node->function(command.args);
        }
        node = node->next;
    }
    //polecenie w PATH
    int result;
    if ((result = funcExecvp(command.args)) == 127) {
        printf("Nie ma takiego polecenia!\n");
    }
    int i = 0;
    while (command.args[i] != NULL) {
        free(command.args[i++]);
    }
    free(command.args);
    return result;
}

//analiza wpisanego tekstu
command_t parser(char *line) {
    if (line[0] != '\n') {
        addToHistory(line);
    }
    command_t command;
    int wordCount = countWords(line);
    command.args = malloc(wordCount * sizeof(char *));
    for (int i = 0; i < wordCount; i++) {
        command.args[i] = calloc(1024, sizeof(char));
    }
    int wordNumber = 0;
    int characterNumber = 0;
    int i = 0;
    char c = line[i];
    while (c != 0) {
        if (c == ' ' || c == '\n' || c == '\t') {
            wordNumber++;
            characterNumber = 0;
        } else {
            command.args[wordNumber][characterNumber] = c;
            characterNumber++;
        }
        c = line[++i];
    }
    command.command = command.args[0];
    command.args[wordCount] = NULL;
    return command;
}

//Funkcje pomocnicze:

//Liczenie wpisanych slow.
int countWords(const char *line) {
    int count = 0;
    int position = 0;
    char c = line[position];
    while (c != 0) {
        if (c == ' ' || line[position + 1] == 0) {
            count++;
        }
        c = line[++position];
    }
    return count;
}

//Dopisywanie polecenia do pliku historii.
void addToHistory(char *line) {
    history = fopen(historyDirectory, "a+");
    lastHistoryNumber++;
    fprintf(history, "%d %s", lastHistoryNumber, line);
    fclose(history);
}

//Wypisywanie historii z zakresu (polecenie fc).
void printHistoryRange(int begin, int end) {
    FILE *file = fopen(historyDirectory, "r");
    char readLine[1024];
    while (fgets(readLine, 1024, file)) {
        char number[1024];
        unsigned long length =  strchr(readLine, ' ') - readLine;
        memcpy(number, readLine,length);
        number[length] = 0;
        int lineNumber = atoi(number);
        if (lineNumber >= begin && lineNumber <= end) {
            printf(readLine);
        }
    }
    fclose(file);
}

//Zapisywanie historii z zakresu do pliku (polecenie fc).
void saveHistoryRange(int begin, int end) {
    FILE *file = fopen(historyDirectory, "r");
    char randString[6];
    srand(time(NULL));
    for (int j = 0; j < 6; j++) {
        randString[j] = rand() % (122 + 1 - 97) + 97;
    }
    char name[19];
    strcpy(name, "/tmp/mysh-fc.");
    strcat(name, randString);
    name[19] = 0;
    FILE *FCFile = fopen(name, "a");

    char readLine[1024];
    while (fgets(readLine, 1024, file)) {
        char number[1024];
        unsigned long length = strchr(readLine, ' ') - readLine;
        memcpy(number, readLine, length);
        number[length] = 0;
        int lineNumber = atoi(number);
        if (lineNumber >= begin && lineNumber <= end) {
            fprintf(FCFile, strchr(readLine, ' ') + 1);
        }
    }
    fclose(file);
    fclose(FCFile);
    char *args[2];
    args[0] = "nano";
    args[1] = name;
    args[2] = NULL;
    funcExecvp(args);
    executeFile(name);

}

//Wykonywanie polecen po edycji (polecenie fc).
void executeFile(char* path) {
    FILE *file = fopen(path, "r");
    char readLine[1024];
    while (fgets(readLine, 1024, file)) {
        printf(readLine);
        commandFinder(parser(readLine));
    }
}

//Sprawdzanie czy argument to liczba (polecenie fc).
int isNumber(char *string) {
    if (string == NULL) {
        return 0;
    }
    int length = strlen(string);
    for (int i = 0; i < length; i++) {
        if (!isdigit(string[i])) {
            return 0;
        }
    }
    return 1;
}

//Znak zachety
void printCommandPrompt() {
    printf("%s@%s:%s$ ", username, hostname, pwd);
}

int main() {

    gethostname(hostname, 1024);
    username = getUserName();
    getcwd(pwd, 1024);
    initHistory();

    node_t exit;
    node_t version;
    node_t cd;
    node_t help;
    node_t history;
    node_t fc;
    initNode(&exit, "exit", &funcExit, &version);
    initNode(&version, "version", &funcVersion, &cd);
    initNode(&cd, "cd", &funcCD, &help);
    initNode(&help, "help", &funcHelp, &history);
    initNode(&history, "history", &funcHistory, &fc);
    initNode(&fc, "fc", &funcFC, NULL);

    commandListHead = &exit;

    while (1) {
        printCommandPrompt();
        commandFinder(parser(readLine()));
    }
}